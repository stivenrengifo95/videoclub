<?php

namespace App\Entity;

use App\Repository\MovieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MovieRepository::class)
 */
class Movie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="movies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=TypeMovie::class, inversedBy="movies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type_movie;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToMany(targetEntity=Rental::class, inversedBy="movies")
     */
    private $rentals;

    /**
     * Movie constructor.
     */
    public function __construct()
    {
        $this->rentals = new ArrayCollection();
    }

    /**
    * Getters and Setters methods
    */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = ucfirst($name);

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getTypeMovie(): ?TypeMovie
    {
        return $this->type_movie;
    }

    public function setTypeMovie(?TypeMovie $type_movie): self
    {
        $this->type_movie = $type_movie;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTime $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTime $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return Collection|Rental[]
     */
    public function getRentals(): Collection
    {
        return $this->rentals;
    }

    public function addRental(Rental $rental): self
    {
        if (!$this->rentals->contains($rental)) {
            $this->rentals[] = $rental;
        }

        return $this;
    }

    public function removeRental(Rental $rental): self
    {
        $this->rentals->removeElement($rental);

        return $this;
    }

    /**
     * Get data model Movie
     *
     * @return array
     */
    public function toArray() {

        return [
            'id'         => $this->getId(),
            'name'       => $this->getName(),
            'type_movie' => $this->getTypeMovie()->getName(),
            'category'   => $this->getCategory()->getName(),
            'created_at' => $this->getCreatedAt(),
        ];
    }
}
