<?php

namespace App\Entity;

use App\Repository\LoyaltyPointRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LoyaltyPointRepository::class)
 */
class LoyaltyPoint
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="loyaltyPoints")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer;

    /**
     * @ORM\ManyToOne(targetEntity=Rental::class, inversedBy="loyaltyPoints")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rental;

    /**
     * Getters and Setters methods
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getRental(): ?Rental
    {
        return $this->rental;
    }

    public function setRental(?Rental $rental): self
    {
        $this->rental = $rental;

        return $this;
    }

    /**
     * Get data model Movie
     *
     * @return array
     */
    public function toArray() {

        return [
            'id'         => $this->getId(),
            'points'     => $this->getValue(),
        ];
    }
}
