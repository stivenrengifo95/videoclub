<?php

namespace App\Entity;

use App\Repository\TypeMovieRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeMovieRepository::class)
 */
class TypeMovie
{
    const UNIT_PRICE = 3;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", precision=10, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;

    /**
     * @ORM\Column(type="integer")
     */
    private $rental_days;

    /**
     * @ORM\Column(type="integer")
     */
    private $loyaltyPoint;

    /**
    * Getters and Setters methods
    */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getRentalDays(): ?int
    {
        return $this->rental_days;
    }

    public function setRentalDays(int $rental_days): self
    {
        $this->rental_days = $rental_days;

        return $this;
    }

    public function getLoyaltyPoint(): ?int
    {
        return $this->loyaltyPoint;
    }

    public function setLoyaltyPoint(int $loyaltyPoint): self
    {
        $this->loyaltyPoint = $loyaltyPoint;

        return $this;
    }
}
