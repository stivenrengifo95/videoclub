<?php

namespace App\Entity;

use App\Repository\RentalRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RentalRepository::class)
 */
class Rental
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $number_days;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="date")
     */
    private $startDate;

    /**
     * @ORM\Column(type="date")
     */
    private $endDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="rentals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer;

    /**
     * @ORM\ManyToMany(targetEntity=Movie::class, mappedBy="rentals")
     */
    private $movies;

    /**
     * @ORM\OneToMany(targetEntity=LoyaltyPoint::class, mappedBy="rental")
     */
    private $loyaltyPoints;


    /**
     * Rental constructor.
     */
    public function __construct()
    {
        $this->movies        = new ArrayCollection();
        $this->loyaltyPoints = new ArrayCollection();
    }

    /**
    * Getters and Setters methods
    */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumberDays(): ?int
    {
        return $this->number_days;
    }

    public function setNumberDays(int $number_days): self
    {
        $this->number_days = $number_days;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTime $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTime $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection|Movie[]
     */
    public function getMovies(): Collection
    {
        return $this->movies;
    }

    public function addMovie(Movie $movie): self
    {
        if (!$this->movies->contains($movie)) {
            $this->movies[] = $movie;
            $movie->addRental($this);
        }

        return $this;
    }

    public function removeMovie(Movie $movie): self
    {
        if ($this->movies->removeElement($movie)) {
            $movie->removeRental($this);
        }

        return $this;
    }

    /**
     * @return Collection|LoyaltyPoint[]
     */
    public function getLoyaltyPoints(): Collection
    {
        return $this->loyaltyPoints;
    }

    public function addLoyaltyPoint(LoyaltyPoint $loyaltyPoint): self
    {
        if (!$this->loyaltyPoints->contains($loyaltyPoint)) {
            $this->loyaltyPoints[] = $loyaltyPoint;
            $loyaltyPoint->setRental($this);
        }

        return $this;
    }

    public function removeLoyaltyPoint(LoyaltyPoint $loyaltyPoint): self
    {
        if ($this->loyaltyPoints->removeElement($loyaltyPoint)) {
            // set the owning side to null (unless already changed)
            if ($loyaltyPoint->getRental() === $this) {
                $loyaltyPoint->setRental(null);
            }
        }

        return $this;
    }

    /**
     * Get data model Rental
     *
     * @return array
     */
    public function toArray() {

        return [
            'id'          => $this->getId(),
            'customer'    => $this->getCustomer()->getName(),
            'price'       => $this->getPrice(),
            'start_date'  => $this->getStartDate(),
            'end_date'    => $this->getEndDate(),
            'number_days' => $this->getNumberDays(),
            'created_at'  => $this->getCreatedAt(),
        ];
    }
}
