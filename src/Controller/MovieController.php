<?php

namespace App\Controller;

use App\Repository\MovieRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class MovieController
 * @package App\Controller
 * @author Stiven Rengifo <stiven95rengifo@gmail.com>
 */
class MovieController extends ApiController
{
    private $movieRepository;

    /**
     * MovieController constructor.
     * @param MovieRepository $movieRepository
     */
    public function __construct( MovieRepository $movieRepository )
    {
        $this->movieRepository = $movieRepository;
    }

    /**
     * Get the list of movies
     *
     * @param Request $request
     * @return JsonResponse
     *
     * Created by <Engineer>
     * User:       <Stiven Rengifo>
     * Email:      <stiven95rengifo@gmail.com>
     * Date:       04/09/21
     */
    public function getAll( Request $request ): JsonResponse
    {
        try{
                $movies = $request->get('type')
                                                ? $this->movieRepository->findBy( ['type_movie' => $request->get('type')] )
                                                : $this->movieRepository->findAll();

            foreach ($movies as $key => $out)
                $data[] = $out->toArray();

            return $this->showAll($data ?? [], Response::HTTP_OK);

        } catch (\Exception $exception){

            return $this->errorResponse($exception->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }
}
