<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Entity\LoyaltyPoint;
use App\Entity\Movie;
use App\Entity\Rental;
use App\Repository\RentalRepository;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RentalController
 * @package App\Controller
 * @author Stiven Rengifo <stiven95rengifo@gmail.com>
 */
class RentalController extends ApiController
{

    private $rentalRepository;

    /**
     * RentalController constructor.
     * @param RentalRepository $rentalRepository
     */
    public function __construct( RentalRepository $rentalRepository )
    {
        $this->rentalRepository = $rentalRepository;
    }

    /**
     * Movie rental creation
     *
     * @param Request $request
     * @return JsonResponse
     * Created by <Engineer>
     * User:      <Stiven Rengifo>
     * Email:     <stiven95rengifo@gmail.com>
     * Date:      04/09/21
     */
    public function create( Request $request )
    {
        try{

            $entityManager    = $this->getDoctrine()->getManager();
            $data             = json_decode( $request->getContent(),TRUE )['data'];
            $customer         = $entityManager->getRepository(Customer::class)->find( $data['customerId'] );
            $endDate          = Carbon::parse( $data['endDate'] );
            $numberDays       = $endDate->diffInDays( Carbon::now() ) + 1 ;
            $dataRentalMovies = self::calculatePriceRentalAndloyaltyPoint( $numberDays, $data['movies'] );

            $rental = new Rental();
            $rental->setCustomer( $customer );
            $rental->setPrice( $dataRentalMovies['priceRental'] );
            $rental->setStartDate( Carbon::now()->toDate() );
            $rental->setEndDate( $endDate->toDate() );
            $rental->setNumberDays( $numberDays );
            $rental->setCreatedAt( new \DateTime() );
            $rental->setUpdatedAt( new \DateTime() );
            $entityManager->persist( $rental );

            foreach ( $data['movies'] as $id )
                $rental->addMovie( $entityManager->getRepository(Movie::class)->find($id) );

            $loyaltyPoint = new LoyaltyPoint();
            $loyaltyPoint->setRental( $rental );
            $loyaltyPoint->setCustomer( $customer );
            $loyaltyPoint->setValue( $dataRentalMovies['loyaltyPoint'] );
            $entityManager->persist( $loyaltyPoint );
            $entityManager->flush();

            return $this->showOne( $rental->toArray() );

        }catch (\Exception $exception){

            return $this->errorResponse( $exception->getMessage(), Response::HTTP_BAD_REQUEST );
        }
    }

    /**
     * Calculate the rental price and loyalty points
     *
     * @param $numberDays
     * @param $movies
     * @return float|int
     *
     * Created by <Engineer>
     * User:      <Stiven Rengifo>
     * Email:     <stiven95rengifo@gmail.com>
     * Date:      04/09/21
     */
    public function calculatePriceRentalAndloyaltyPoint( $numberDays, $movies ){

        $em          = $this->getDoctrine()->getManager();
        $rentalPrice = $loyaltyPoint = 0;

        foreach ( $movies as $id ){

            $movie           = $em->getRepository(Movie::class)->find( $id );
            $typeMovie       = $movie->getTypeMovie();
            $priceTypeMovie  = $typeMovie->getPrice();
            $loyaltyPoint   += $typeMovie->getLoyaltyPoint();

            switch ( $typeMovie->getName() ){
                case 'standard_movies':
                case 'old_movies':
                    $rentalPrice += $priceTypeMovie * $typeMovie->getRentalDays();

                    if( $numberDays > $typeMovie->getRentalDays() )
                        $rentalPrice += $priceTypeMovie * ( $numberDays - $typeMovie->getRentalDays() );
                    break;

                default:
                    $rentalPrice += $priceTypeMovie * $numberDays;
            }
        }

        return [
            'priceRental'  => $rentalPrice,
            'loyaltyPoint' => $loyaltyPoint
        ];
    }
}
