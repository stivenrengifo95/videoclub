<?php

namespace App\Controller;

use App\Repository\LoyaltyPointRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LoyaltyPointController
 * @package App\Controller
 * @author Stiven Rengifo <stiven95rengifo@gmail.com>
 */
class LoyaltyPointController extends ApiController
{
    private $loyaltyPointRepository;

    /**
     * LoyaltyPointController constructor.
     * @param LoyaltyPointRepository $loyaltyPointRepository
     */
    public function __construct( LoyaltyPointRepository $loyaltyPointRepository )
    {
        $this->loyaltyPointRepository = $loyaltyPointRepository;
    }

    /**
     * Get loyalty points of a Customer
     * @param $customerId
     * @return JsonResponse
     *
     * Created by <Engineer>
     * User:      <Stiven Rengifo>
     * Email:     <stiven95rengifo@gmail.com>
     * Date:      04/09/21
     */
    public function getLoyaltyPointsOfCustomer( $customerId )
    {
        try{

            $loyaltyPoints = $this->loyaltyPointRepository->findBy( ['customer' => $customerId ] );

            foreach ( $loyaltyPoints as $out )
                $data[] = $out->toArray();

            return $this->showAll( $data??[], Response::HTTP_OK );

        }catch (\Exception $exception){

            return $this->errorResponse( $exception->getMessage(), Response::HTTP_BAD_REQUEST );
        }
    }
}
