<?php


namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CategoryFixtures extends Fixture
{
    private $faker;

    public const CATEGORY_REFERENCE = 'category';

    /**
     * MovieFixtures constructor.
     */
    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        $categories = [
            'Action',
            'Comedy',
            'Drama',
            'Fantasy',
            'Horror',
            'Mystery',
            'Romance',
            'Thriller',
        ];

        foreach ( $categories as $key => $out ){
            $category = new Category();
            $category->setName( $out );
            $category->setStatus( TRUE );
            $category->setCreatedAt( new \DateTime() );
            $category->setUpdatedAt( new \DateTime() );
            $manager->persist( $category );

            $this->addReference( self::CATEGORY_REFERENCE.($key+1), $category );
        }

        $manager->flush();
    }
}