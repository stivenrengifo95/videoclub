<?php

namespace App\DataFixtures;

use App\Entity\Movie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class MovieFixtures extends Fixture implements DependentFixtureInterface
{
    private $faker;

    /**
     * MovieFixtures constructor.
     */
    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        for ( $i = 0; $i < 10; $i++ ){
            $movie = new Movie();
            $movie->setName( $this->faker->firstNameMale );
            $movie->setTypeMovie( $this->getReference( TypeMovieFixtures::TYPE_MOVIE_REFERENCE.rand(1,3) ));
            $movie->setCategory( $this->getReference( CategoryFixtures::CATEGORY_REFERENCE.rand(1,8) ) );
            $movie->setCreatedAt( new \DateTime() );
            $movie->setUpdatedAt( new \DateTime() );
            $manager->persist($movie);
        }

        $manager->flush();
    }

    /**
     * Loading the accessory files in order
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            TypeMovieFixtures::class,
            CategoryFixtures::class,
        ];
    }
}