<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CustomerFixtures extends Fixture
{

    private $faker;

    /**
     * CustomerFixtures constructor.
     */
    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        for ( $i = 0; $i < 10; $i++ ){
            $customer = new Customer();
            $customer->setName( $this->faker->name );
            $customer->setCreatedAt( new \DateTime() );
            $customer->setUpdatedAt( new \DateTime() );
            $manager->persist($customer);
        }

        $manager->flush();
    }

    /**
     * Loading the accessory files in order
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            Customer::class,
            TypeMovieFixtures::class,
            CategoryFixtures::class,
        ];
    }
}