<?php

namespace App\DataFixtures;

use App\Entity\TypeMovie;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class TypeMovieFixtures extends Fixture
{
    public const TYPE_MOVIE_REFERENCE = 'type-movie';

    /**
     * @param ObjectManager $manager
     *
     * Created by <Engineer>
     * User:       <Stiven Rengifo>
     * Email:      <stiven95rengifo@gmail.com>
     * Date:       04/09/21
     */
    public function load(ObjectManager $manager)
    {
        $rentalDays = 1;
        $typeMovies = [
            'new_launches',
            'standard_movies',
            'old_movies'
        ];

        foreach( $typeMovies as $key => $out ){
            $typeMovie = new TypeMovie();
            $typeMovie->setName( $out );
            $typeMovie->setPrice( TypeMovie::UNIT_PRICE );
            $typeMovie->setRentalDays( $rentalDays );
            $typeMovie->setLoyaltyPoint( $out == 'new_launches'? 2 : 1 );
            $typeMovie->setStatus( TRUE );
            $manager->persist( $typeMovie );

            $this->addReference(self::TYPE_MOVIE_REFERENCE.($key+1), $typeMovie);
            $rentalDays += 2;
        }

        $manager->flush();
    }
}