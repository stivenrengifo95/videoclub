<?php

namespace App\Repository;

use App\Entity\TypeMovie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeMovie|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeMovie|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeMovie[]    findAll()
 * @method TypeMovie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeMovieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeMovie::class);
    }

    // /**
    //  * @return TypeMovie[] Returns an array of TypeMovie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeMovie
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
